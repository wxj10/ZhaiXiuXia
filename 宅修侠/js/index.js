$(".header-nav ul li").click(function() {
	var index = $(this).index(); //获取当前点击的li是ul中的第几个
	console.log(index)
	if (index == 0) {
		window.location.href = 'index.html';
	} else if (index == 1) {
		window.location.href = 'page1.html'
	} else if (index == 2) {
		window.location.href = 'page2.html'
	} else if (index == 3) {
		window.location.href = 'page3.html'
	} else if (index == 4) {
		window.location.href = 'page4.html'
	} else if (index == 5) {
		window.location.href = 'page5.html'
	} else if (index == 6) {
		window.location.href = 'page6.html'
	}
})

var swiper = new Swiper('.swiper-container', {
	slidesPerView: 1,
	spaceBetween: 0,
	loop: true,
	pagination: {
		el: '.swiper-pagination',
		clickable: true,
	},
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
});
